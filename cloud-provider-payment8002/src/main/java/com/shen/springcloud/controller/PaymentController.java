package com.shen.springcloud.controller;

import com.shen.springcloud.pojo.CommonResult;
import com.shen.springcloud.pojo.Payment;
import com.shen.springcloud.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@Slf4j
public class PaymentController {
    @Resource
    private PaymentService paymentService;

    @Value("${server.port}")
    private String severPort;

    @PostMapping(value = "/payment/create")
    public CommonResult create(@RequestBody Payment payment){
        int result = paymentService.create(payment);
        log.info("DeBug====>" + result);
        if (result > 0){
            return new CommonResult(200,"插入数据库成功,severPost:" + severPort);
        }else{
            return new CommonResult(444,"插入数据库失败",null);
        }
    }

    @GetMapping(value = "/payment/get/{id}")
    public CommonResult paymentById(@PathVariable("id") Long id){
        System.out.println("Hello");
        Payment payment = paymentService.getPaymentById(id);
        log.info("DeBug==>" + payment);
        if (payment != null){
             return new CommonResult(200,"查询成功,serverPost:" + severPort,payment);
        }else{
            return new CommonResult(444,"没有对应的记录，查询Id" + id,null);
        }
    }

    @GetMapping(value = "/payment/lb")
    public String getPaymentLb(){
        return severPort;
    }
}
