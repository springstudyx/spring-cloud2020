package com.shen.springcloud.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @program: SpringCloud
 * @description:
 * @author: Mr.shen
 * @create: 2020-07-24 16:11
 **/
@FeignClient(value = "CLOUD-PROVIDER-HYSTRIX-PAYMENT",fallback = PaymentFallbackService.class)
public interface PaymentHystrixService {
    /**
     *正常访问
     * @param id
     * @return
     */
    @RequestMapping(value = "/payment/hystrix/ok/{id}",method = RequestMethod.GET)
    public String paymentInfo_OK(@PathVariable("id") Integer id);

    /**
     * 超时
     * @param id
     * @return
     */
    @RequestMapping(value = "/payment/hystrix/timeout/{id}",method = RequestMethod.GET)
    public String paymentInfo_TimeOut(@PathVariable("id") Integer id);
}
