package com.shen.springcloud.service;

import org.springframework.stereotype.Component;

/**
 * @program: SpringCloud
 * @description:
 * @author: Mr.shen
 * @create: 2020-07-25 16:44
 **/
@Component
public class PaymentFallbackService implements PaymentHystrixService {
    @Override
    public String paymentInfo_OK(Integer id) {
        return "----paymentFallbackService fall back-paymentInfo_OK  /(ㄒoㄒ)/~~";
    }

    @Override
    public String paymentInfo_TimeOut(Integer id) {
        return "---PaymentFallbackService fallback-paymentInfo_TimeOut /(ㄒoㄒ)/~~";
    }
}
