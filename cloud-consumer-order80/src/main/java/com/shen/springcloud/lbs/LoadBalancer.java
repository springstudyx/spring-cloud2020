package com.shen.springcloud.lbs;

import org.springframework.cloud.client.ServiceInstance;

import java.util.List;

/**
 * @program: SpringCloud
 * @description:
 * @author: Mr.shen
 * @create: 2020-07-22 14:40
 **/
public interface LoadBalancer {
    /**
     * 获得
     * @param serviceInstances
     * @return
     */
    ServiceInstance instances(List<ServiceInstance> serviceInstances);
}
