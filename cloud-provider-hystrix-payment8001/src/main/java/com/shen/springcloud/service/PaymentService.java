package com.shen.springcloud.service;

/**
 * @program: SpringCloud
 * @description:
 * @author: Mr.shen
 * @create: 2020-07-23 19:39
 **/
public interface PaymentService {
    /**
     *正常访问
     * @param id
     * @return
     */
    String paymentInfo_OK(Integer id);

    /**
     * 超时
     * @param id
     * @return
     */
    String paymentInfo_Timeout(Integer id);

    /**
     * 服务熔断演示
     * @param id
     * @return
     */
    String paymentCircuitBreaker(Integer id);
}
